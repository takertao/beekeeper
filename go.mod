module github.com/ethersphere/beekeeper

go 1.14

require (
	github.com/ethersphere/bee v0.0.0-20200506182550-8e958f02081d
	github.com/ethersphere/bmt v0.1.1
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.6.3
	golang.org/x/crypto v0.0.0-20200323165209-0ec3e9974c59
)
